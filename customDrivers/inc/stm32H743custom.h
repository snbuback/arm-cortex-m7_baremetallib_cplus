/*
 * stm32H743custom.h
 *
 *  Created on: 10 Feb 2020
 *      Author: Erick Cabral
 */

#ifndef INC_STM32H743CUSTOM_H_
#define INC_STM32H743CUSTOM_H_

#include<cstdint>

#define __vol volatile
#define __vol_32bits_ptr *(volatile unsigned long int)

/* CUSTOIM STM32H743 DRIVER */

/*!< DEFAULT VARIABLES >*/
#define SET				1
#define RESET			0
#define ENABLE			SET
#define DISABLE			DISABLE
#define HIGH			SET
#define LOW				RESET

#define PIN_0			0
#define PIN_1			1
#define PIN_2			2
#define PIN_3			3
#define PIN_4			4
#define PIN_5			5
#define PIN_6			6
#define PIN_7			7
#define PIN_8			8
#define PIN_9			9
#define PIN_10			10
#define PIN_11			11
#define PIN_12			12
#define PIN_13			13
#define PIN_14			14
#define PIN_15			15

/*!< AHB4 BUS PERIPHERALS >*/
#define RCC_BASEADDRESS					0x58024400U

/*!< BASE ADDRESSES DEFAULT >*/

#define APB1_BASEADDRESS				0x40000000U
#define APB2_BASEADDRESS				0x40010000U
#define APB3_BASEADDRESS				0x50000000U
#define APB4_BASEADDRESS				0x58000000U

#define AHB1_BASEADDRESS				0x40017400U
#define AHB2_BASEADDRESS				0x48022800U
#define AHB3_BASEADDRESS				0x51000000U
#define AHB4_BASEADDRESS				0x58020000U

class RCC {
private:
	uint32_t rcc_baseAddress;__vol uint32_t* pABH4ENR =
			(uint32_t*) (rcc_baseAddress + 0x0E);

public:
	RCC(uint32_t rcc_baseAddr) {
		this->rcc_baseAddress = rcc_baseAddr;
	}
	void setABH4ENR(uint32_t abh4_portValue) {
		this->pABH4ENR = (uint32_t*) (this->rcc_baseAddress + 0x0E0);
		*(this->pABH4ENR) = 0x00000000; // RESET
		*(this->pABH4ENR) = abh4_portValue;
	}
};

/*!< GPIO STRUCTURE > */
#define GPIOK_BASEADDRES 				0x58022800U
#define GPIOJ_BASEADDRES				0x58022400U
#define GPIOI_BASEADDRES 				0x58022000U
#define GPIOH_BASEADDRES 				0x58021C00U
#define GPIOG_BASEADDRES 				0x58021800U
#define GPIOF_BASEADDRES 				0x58021400U
#define GPIOE_BASEADDRES 				0x58021000U
#define GPIOD_BASEADDRES 				0x58020C00U
#define GPIOC_BASEADDRES 				0x58020800U
#define GPIOB_BASEADDRES				0x58020400U
#define GPIOA_BASEADDRES 				0x58020000U

#define GPIOA							0
#define GPIOB							1
#define GPIOC							2
#define GPIOD							3
#define GPIOE							4
#define GPIOF							5
#define GPIOG							6
#define GPIOH							7
#define GPIOI							8
#define GPIOJ							9
#define GPIOK							10

/*!< MODER OPTIONS >*/
//Reset value:
// 0xABFF FFFF for port A
// 0xFFFF FEBF for port B
// 0xFFFF FFFF for other ports
#define MODE_RESET_PORTA 	0xABFFFFFF // 0xFFFF FEBF for port A
#define MODE_RESET_PORTB 	0xFFFFFEBF // 0xFFFF FEBF for port B
#define MODE_RESET_OTHERS 	0xFFFFFFFF // 0xFFFF FEBF other

#define MODE_INPUT			0	//0b00: Input mode    0b0000 0000
#define	MODE_OUTPUT			1	//0b01: General purpose output mode
#define	MODE_ALT_FUNC		2	//0b10: Alternate function mode
#define	MODE_ANALOG			3	//0b11: Analog mode (reset state)

/*!< TYPER OPTIONS >*/
#define TYPER_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000
#define TYPE_PUSH_PULL		0	//0: Output push-pull (reset state)
#define	TYPE_OPEN_DRAIN		1	//1: Output open-drain

/*!< SPEEDR OPTIONS >*/
#define SPEED_RESET_PORTA 	0x0C000000 //Reset value: 0x0C00 0000 (for port A)
#define SPEED_RESET_PORTB 	0x000000C0 //Reset value: 0x0000 00C0 (for port B)
#define SPEED_RESET_OTHERS 	0x00000000 //Reset value: 0x0000 0000 (for other ports)

#define SPEED_LOW		0	//00: Low speed
#define	SPEED_MEDIUM	1	//01: Medium speed
#define	SPEED_HIGH		2	//10: High speed
#define	SPEED_VHIGH		3	//11: Very high speed

/*!< PUPD OPTIONS >*/
#define PUPD_RESET_PORTA 	 0x64000000 //Reset value: 0x6400 0000 (for port A)
#define PUPD_RESET_PORTB 	 0x00000100 //Reset value: 0x0000 0100 (for port B)
#define PUPD_RESET_OTHERS 	 0x00000000 //Reset value: 0x0000 0000 (for other ports)

#define PUPD_DISABLED		0	//00: No pull-up, pull-down
#define	PUPD_UP				1	//01: Pull-up
#define	PUPD_DOWN			2	//10: Pull-down
#define	PUPD_RESERVED		3	//11: Reserved

/*!< IDR OPTIONS >*/
#define IDR_RESET_PORT 	0x00000000 //Reset value: 0x0000 XXXX

/*!< ODR OPTIONS >*/
#define ODR_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

/*!< BSRR OPTIONS >*/
#define BSRR_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000
//Bits [31-16] OPTIONS
//0: No action on the corresponding ODRx bit
//1: Resets the corresponding ODRx bit
//Bits [15-0] OPTIONS
//0: No action on the corresponding ODRx bit
//1: Sets the corresponding ODRx bit

/*!< LCKR OPTIONS >*/
#define LCKR_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

/*!< AFRL OPTIONS >*/
#define AFRL_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

#define AF0			0 	//0000:	AF0
#define AF1			1 	//0001:	AF1
#define AF2			2 	//0010:	AF2
#define AF3			3 	//0011:	AF3
#define AF4			4 	//0100:	AF4
#define AF5			5 	//0101:	AF5
#define AF6			6 	//0110: AF6
#define AF7			7 	//0111: AF7
#define AF8			8 	//1000: AF8
#define AF9			9 	//1001: AF9
#define AF10		10 	//1010: AF10
#define AF11		11 	//1011: AF11
#define AF12		12 	//1100: AF12
#define AF13		13 	//1101: AF13
#define AF14		14 	//1110: AF14
#define AF15		15 	//1111: AF15

/*!< AFRH OPTIONS >*/
#define AFRH_RESET_PORT 	0x00000000 //Reset value: 0x0000 0000

class GPIOx {
private:
	uint32_t gpio_x_baseAddress;
	uint32_t* pMODER;
	uint32_t* pTYPE;
	uint32_t* pSPEED;
	uint32_t* pPUPD;
	uint32_t* pIDR;
	uint32_t* pODR;
	uint32_t* pBSRR;
	uint32_t* pLCKR;
	uint32_t* pAFRL;
	uint32_t* pAFRH;

public:
	GPIOx(uint32_t gpio_baseAddress);
	void setGPIOx_MODE(uint8_t portValue);
	void setPINx_MODE(uint8_t pinNumber, uint8_t mode);

	void setPINx_TYPE(uint8_t pinNumber, uint8_t mode);

	void setPINx_SPEED(uint8_t pinNumber, uint8_t mode);

	void setPINx_PUPD(uint8_t pinNumber, uint8_t mode);

	uint8_t PINx_IDR(uint8_t pinNumber, uint8_t mode);

	void setPINx_ODR(uint8_t pinNumber, uint8_t mode);
};

//====== SUPPORT FUNCTIONS ======== //

void setTwoBitsRegister(volatile uint32_t *registerToSet, uint8_t bitToSet,
		uint8_t valueToSet);
void setOneBitRegister(volatile uint32_t *registerToSet, uint8_t bitToSet,
		uint8_t valueToSet);
#endif /* INC_STM32H743CUSTOM_H_ */
